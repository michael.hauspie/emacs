# Emacs configuration

My emacs configuration is now all done using `org-mode` and is
documented directly in `org-mode`. See
[configuration.org](configuration.org) file.
