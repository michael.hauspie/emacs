;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Author: Michaël Hauspie
;; Time-stamp: "2020-08-20 15:45:40 (hauspie)"
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Simple oneliner that loads the configuration written in org-mode

(org-babel-load-file "~/.emacs.d/configuration.org")

;; Local Variables:
;; mode: emacs-lisp
;; End:
